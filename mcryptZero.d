module mcryptZero;

private ubyte roR(ubyte b)
{
	if ((b % 2) == 0)
	{
		return cast(ubyte)(b >>> 1);
	}

	return cast(ubyte)((b >>> 1) + 128);
}

private ubyte roL(ubyte b)
{
	if (b < 128)
	{
		return cast(ubyte)(b << 1);
	}

	return cast(ubyte)((b << 1) + 1);
}

private ubyte encode8(ubyte d, ubyte k)
{
	for(int i=0; i<8; i++)
	{
		if((k & 128) == 0)
		{
			d = cast(ubyte)(roL(d) + 4);
		} else
		{
			d = cast(ubyte)(roL(d) - 3);
		}
		k = cast(ubyte)(k << 1);
	} 
	return d;
}

private ubyte decode8(ubyte e, ubyte k)
{
	for(int i=0; i<8; i++)
	{
		if((k & 1) == 0)
		{
			e = roR(cast(ubyte)(e - 4));
		} else
		{
			e = roR(cast(ubyte)(e + 3));
		}
		k = cast(ubyte)(k >>> 1);
	}
	return e;
}

ubyte[] encryptZero(ubyte[] data, ubyte[] key)
{
	ubyte[] result = new ubyte[data.length];
	ubyte dummy = 0;
	ubyte dummy2 = 0;
	ubyte dummy3 = 0;
	ubyte dummy4 = 0;
	for(int i=0; i<data.length; i++)
	{
		result[i] = data[i];
		result[i] = encode8(result[i], dummy);
		result[i] = encode8(result[i], dummy2);
		result[i] = encode8(result[i], dummy3);
		result[i] = encode8(result[i], dummy4);
		for(int j=0; j<key.length; j++)
		{
			ubyte k = key[j];
			result[i] = encode8(result[i], k);
		}
		dummy4 = dummy3;
		dummy3 = dummy2;
		dummy2 = dummy;
		dummy = result[i];
	}
	return result;
}

ubyte[] decryptZero(ubyte[] data, ubyte[] key)
{
	ubyte[] result = new ubyte[data.length];
	ubyte dummy = 0;
	ubyte dummy2 = 0;
	ubyte dummy3 = 0;
	ubyte dummy4 = 0;
	ubyte ndummy = 0;
	for(int i=0; i<data.length; i++)
	{
		result[i] = data[i];
		ndummy = data[i];
		for(int j=cast(int)key.length - 1; j>=0; j--)
		{
			ubyte k = key[j];
			result[i] = decode8(result[i], k);
		}
		result[i] = decode8(result[i], dummy4);
		result[i] = decode8(result[i], dummy3);
		result[i] = decode8(result[i], dummy2);
		result[i] = decode8(result[i], dummy);
		dummy4 = dummy3;
		dummy3 = dummy2;
		dummy2 = dummy;
		dummy = ndummy;
	}
	return result;
}