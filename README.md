# mcryptZero
*This project is under experiment and development - not ready to use !!!*

**Example Usage**
```
import mcryptZero;
import std.stdio;

void main()
{
	string data = "This is data...";
	string key = "secret";
	writeln("data      = ",data);
	writeln("key       = ", key);
	ubyte[] encrypt = encryptZero(cast(ubyte[])data, cast(ubyte[])key);
	writeln("encrypt   = ",encrypt);
	string databack = cast(string)decryptZero(encrypt, cast(ubyte[])key);
	writeln("data back = ",databack);
}
```
